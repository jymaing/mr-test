# MR Test

Merge Request Test

[TOC]

## Test Cases

- Merge without Squash
  - Without master change
    - Without change same file
      - Without merge from another branch
        - Two branches from same commit
        - Two branches from different commit
      - With merge from another branch
        - Two branches from same commit
        - Two branches from different commit
    - With change same file
      - Without merge from another branch
        - Two branches from same commit
        - Two branches from different commit
      - With merge from another branch
        - Two branches from same commit
        - Two branches from different commit
  - With master change
    - Without change same file
      - Without merge from another branch
        - Two branches from same commit
        - Two branches from different commit
      - With merge from another branch
        - Two branches from same commit
        - Two branches from different commit
    - With change same file
      - Without merge from another branch
        - Two branches from same commit
        - Two branches from different commit
      - With merge from another branch
        - Two branches from same commit
        - Two branches from different commit
- Merge with Squash
  - Without master change
    - Without change same file
      - Without merge from another branch
        - Two branches from same commit
        - Two branches from different commit
      - With merge from another branch
        - Two branches from same commit
        - Two branches from different commit
    - With change same file
      - Without merge from another branch
        - Two branches from same commit
        - Two branches from different commit
      - With merge from another branch
        - Two branches from same commit
        - Two branches from different commit
  - With master change
    - Without change same file
      - Without merge from another branch
        - Two branches from same commit
        - Two branches from different commit
      - With merge from another branch
        - Two branches from same commit
        - Two branches from different commit
    - With change same file
      - Without merge from another branch
        - Two branches from same commit
        - Two branches from different commit
      - With merge from another branch
        - Two branches from same commit
        - Two branches from different commit

## Test Result
